import React from 'react';
import { MainWraper, HeaderMobile, ScrollTop } from 'components';
import { AboutMe } from 'pages';

const App = (props) => {
  return (
    <MainWraper>
      <HeaderMobile />
      <AboutMe /> 
      <ScrollTop />
    </MainWraper>
  )
}

export default App;
