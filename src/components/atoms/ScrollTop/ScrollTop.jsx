import React, { Component } from 'react'

export default class ScrollTop extends Component {
    render() {
        return (
            <div id="kt_scrolltop" className="kt-scrolltop">
                <i className="fa fa-arrow-up"></i>
            </div>
        );
    }
}