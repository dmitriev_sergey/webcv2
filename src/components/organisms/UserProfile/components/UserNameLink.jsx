import React, { Component } from 'react'

export default class UserNameLink extends Component {
    render() {
        return (
            <a href="davra.cz" className="kt-widget__username">
                Sergey Dmitriev    
                <i className="flaticon2-correct"></i>                       
            </a>
        )
    }
}