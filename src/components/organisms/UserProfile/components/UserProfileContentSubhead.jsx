import React, { Component } from 'react'

export default class UserProfileContentSubhead extends Component {
    render() {
        return (
            <div className="kt-widget__subhead">
                <a href="davra.cz"><i className="flaticon2-new-email"></i>sergey.dmitriev@davra.cz</a>
                <a href="davra.cz"><i className="flaticon2-calendar-3"></i>[JS]React Developer </a>
                <a href="davra.cz"><i className="flaticon2-placeholder"></i>Prague, Czech Republic</a>
            </div>
        )
    }
}