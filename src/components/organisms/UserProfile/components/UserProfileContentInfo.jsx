import React, { Component } from 'react'

export default class UserProfileContentInfo extends Component {
    render() {
        return (
            <div className="kt-widget__info">
                <div className="kt-widget__desc">
                    Dubito ergo cogito, cogito ergo sum - I doubt, therefore I think, therefore I am 
                    <br /><hr />
                    One of my biggest dreams was programming. I spent much time and made efforts just to get every opportunity to study this subject.
                    <br />I have the greatest respect for the profession such as a programmer. Furthermore, I strongly believe that programming is vital to the modern world.
                    <br />There are many reasons for my decision to start coding, but the main point is – I do love it.
                </div>
            </div>
        )
    }
}