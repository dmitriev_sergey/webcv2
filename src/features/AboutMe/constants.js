const CALCULATOR = {
    name: 'Calculator',
    label: 'Real Estate, construction',
    description: 'This application helps investors to calculate basic financial KPIs of development projects.The client will be able to quicly calculate Return on Investments for every construction he wants to build.',
    fe: 'React, Redux, React-Router',
    be: '---',
    db: 'cloud',
    progress: '10%',
};

const IT_BURSE = {
    name: 'IT-Burse',
    label: 'IT, web-development',
    description: 'Find employees and employers on IT-Burse. Hire remote employers and cut your costs',
    fe: 'React, Redux, React-Router',
    be: 'Node.js, Express',
    db: 'SQL',
    progress: 'ToDo',
}

export const PROJECTS_INFO = [CALCULATOR, IT_BURSE];

export default PROJECTS_INFO;